# CONSTANTS begin with "C_" - DO NOT CHANGE/OVERRIDE THESE VALUES
variable "C_NAMESPACE" {
  description = "Constant value: PutMetricAlarm Parameter representing the namespace for the metric associated with the alarm."
  default = "AWS/ELB"
}
variable "Environment" {
  description = "The target environment (e.g. dev, test, stage, prod)"
}
variable "ApplicationName" {
  description = "Application Name construct: {{appName}} - {{environment}} -{{scope}}[[number]]"
}
variable "ElasticLoadBalancer" {
  description = "The Elastic Load Balancer for the stack"
}
variable "CriticalAlarmTopicARN" {
  description = "The target environment (e.g. The ARN of the Critical Alarm SNS Topic)"
}
variable "MetricName" {
  description = "AWS CloudWatch Metric Name"
  default = "HTTPCode_ELB_4XX"
}
variable "AlarmComparisonOperator" {
  description = "(GreaterThanOrEqualToThreshold, GreaterThanThreshold, LessThanThreshold, LessThanOrEqualToThreshold)"
  default = "GreaterThanOrEqualToThreshold"
}
variable "AlarmEvaluationPeriods" {
  description = "The number of periods over which data is compared to the specified threshold"
  default = "5"
}
variable "AlarmStatistic" {
  description = "The statistic to apply to the alarm's associated metric (SampleCount, Average, Sum, Minimum, Maximum)"
  default = "Average"
}
variable "AlarmThreshold" {
  description = "The value against which the specified statistic is compared"
  default = "75"
}
variable "AlarmPeriod" {
  description = "The time over which the specified statistic is applied. You must specify a time in seconds that is also a multiple of 60"
  default = "60"
}
variable "NamingStandard" {
  description = "HUIT Naming Convention for this resource"
  default = "elb-http-4xx-high-cw-alarm"
}

variable "Map_Of_Metric_Alarms" {
  description = "Defines the list of metrics used to generate each alarm"
  type = "map"
  default = {
    # Next Metric
    MetricName0 = "HTTPCode_ELB_4XX"
    AlarmComparisonOperator0 = "GreaterThanOrEqualToThreshold" #(GreaterThanOrEqualToThreshold, GreaterThanThreshold, LessThanThreshold, LessThanOrEqualToThreshold)
    AlarmEvaluationPeriods0 = "5"
    AlarmStatistic0 = "Average"  #(SampleCount, Average, Sum, Minimum, Maximum)
    AlarmThreshold0 = "75"
    AlarmPeriod0 = "60"
    NamingStandard0 = "elb-http-4xx-high-cw-alarm"
    # Next Metric
    MetricName1 = "HTTPCode_ELB_5XX"
    AlarmComparisonOperator1 = "GreaterThanOrEqualToThreshold" #(GreaterThanOrEqualToThreshold, GreaterThanThreshold, LessThanThreshold, LessThanOrEqualToThreshold)
    AlarmEvaluationPeriods1 = "5"
    AlarmStatistic1 = "Average"  #(SampleCount, Average, Sum, Minimum, Maximum)
    AlarmThreshold1 = "5"
    AlarmPeriod1 = "60"
    NamingStandard1 = "elb-http-5xx-high-cw-alarm"
    # Next Metric
    MetricName2 = "HealthyHostCount"
    AlarmComparisonOperator2 = "GreaterThanOrEqualToThreshold" #(GreaterThanOrEqualToThreshold, GreaterThanThreshold, LessThanThreshold, LessThanOrEqualToThreshold)
    AlarmEvaluationPeriods2 = "10"
    AlarmStatistic2 = "Average"  #(SampleCount, Average, Sum, Minimum, Maximum)
    AlarmThreshold2 = "1"
    AlarmPeriod2 = "60"
    NamingStandard2 = "elb-healthy-host-count-low-cw-alarm"
    # Next Metric
    MetricName3 = "Latency"
    AlarmComparisonOperator3 = "GreaterThanThreshold" #(GreaterThanOrEqualToThreshold, GreaterThanThreshold, LessThanThreshold, LessThanOrEqualToThreshold)
    AlarmEvaluationPeriods3 = "10"
    AlarmStatistic3 = "Average"  #(SampleCount, Average, Sum, Minimum, Maximum)
    AlarmThreshold3 = "20"
    AlarmPeriod3 = "60"
    NamingStandard3 = "elb-latency-high-cw-alarm"
    # Next Metric
    MetricName4 = "RequestCount"
    AlarmComparisonOperator4 = "GreaterThanThreshold" #(GreaterThanOrEqualToThreshold, GreaterThanThreshold, LessThanThreshold, LessThanOrEqualToThreshold)
    AlarmEvaluationPeriods4 = "10"
    AlarmStatistic4 = "Sum"  #(SampleCount, Average, Sum, Minimum, Maximum)
    AlarmThreshold4 = "100"
    AlarmPeriod4 = "60"
    NamingStandard4 = "elb-request-count-high-cw-alarm"
  }
}




