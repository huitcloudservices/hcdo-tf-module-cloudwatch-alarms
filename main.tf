resource "aws_cloudwatch_metric_alarm" "cloudformation-elb-cloudwatch-alarms" {
    alarm_name = "${var.ApplicationName}-${var.Environment}-${var.NamingStandard}"
    alarm_actions = ["${var.CriticalAlarmTopicARN}"]
    alarm_description = "Alarm if ${var.ElasticLoadBalancer} ${var.MetricName} errors are ${var.AlarmComparisonOperator} ${var.AlarmThreshold} for ${var.AlarmEvaluationPeriods} period(s) of ${var.AlarmPeriod}"
    comparison_operator = "${var.AlarmComparisonOperator}"
    dimensions {
        ElasticLoadBalancer = "${var.ElasticLoadBalancer}"
    }
    evaluation_periods = "${var.AlarmEvaluationPeriods}"
    insufficient_data_actions = ["${var.CriticalAlarmTopicARN}"]
    metric_name = "${var.MetricName}"
    namespace = "${var.C_NAMESPACE}"
    ok_actions = ["${var.CriticalAlarmTopicARN}"]
    period = "${var.AlarmPeriod}"
    statistic = "${var.AlarmStatistic}"
    threshold = "${var.AlarmThreshold}"
}

