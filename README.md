# How to use this module

* Copy the **Exhibit A** terraform configuration into your own configuration file
* Edit the variables for your application
* Run ```> terraform get -update``` 
* Run ```> terraform apply```

### (Exhibit A) if you want to use the default settings
```
module "cloudwatch" {
    source = "bitbucket.org/huitcloudservices/hcdo-tf-module-cloudwatch-alarms"
    Environment = "dev" #(dev, test, stage, prod)
    ApplicationName = ""
    ElasticLoadBalancer = ""
    CriticalAlarmTopicARN = ""
}
```

### (Exhibit B) if you want to override settings
```
module "cloudwatch1" {
    source = "bitbucket.org/huitcloudservices/hcdo-tf-module-cloudwatch-alarms"
    Environment = "dev" #(dev, test, stage, prod)
    ApplicationName = ""
    ElasticLoadBalancer = ""
    CriticalAlarmTopicARN = ""
    "MetricName" = "HTTPCode_ELB_4XX"
    "AlarmComparisonOperator" = "GreaterThanOrEqualToThreshold"
    "AlarmEvaluationPeriods" = "5"
    "AlarmStatistic" = "Average"
    "AlarmThreshold" = "75"
    "AlarmPeriod" = "60"
    "NamingStandard" = "elb-http-4xx-high-cw-alarm"

}

module "cloudwatch2" {
    source = "bitbucket.org/huitcloudservices/hcdo-tf-module-cloudwatch-alarms"
    Environment = "dev" #(dev, test, stage, prod)
    ApplicationName = ""
    ElasticLoadBalancer = ""
    CriticalAlarmTopicARN = ""
    "MetricName" = "HTTPCode_ELB_5XX"
    "AlarmComparisonOperator" = "GreaterThanOrEqualToThreshold"
    "AlarmEvaluationPeriods" = "60"
    "AlarmStatistic" = "Average"
    "AlarmThreshold" = "5"
    "AlarmPeriod" = "60"
    "NamingStandard" = "elb-http-5xx-high-cw-alarm"
}

module "cloudwatch3" {
    source = "bitbucket.org/huitcloudservices/hcdo-tf-module-cloudwatch-alarms"
    Environment = "dev" #(dev, test, stage, prod)
    ApplicationName = ""
    ElasticLoadBalancer = ""
    CriticalAlarmTopicARN = ""
    "MetricName" = "HealthyHostCount"
    "AlarmComparisonOperator" = "GreaterThanOrEqualToThreshold"
    "AlarmEvaluationPeriods" = "10"
    "AlarmStatistic" = "Average"
    "AlarmThreshold" = "1"
    "AlarmPeriod" = "60"
    "NamingStandard" = "elb-healthy-host-count-low-cw-alarm"
}

module "cloudwatch4" {
    source = "bitbucket.org/huitcloudservices/hcdo-tf-module-cloudwatch-alarms"
    Environment = "dev" #(dev, test, stage, prod)
    ApplicationName = ""
    ElasticLoadBalancer = ""
    CriticalAlarmTopicARN = ""
    "MetricName" = "Latency"
    "AlarmComparisonOperator" = "GreaterThanThreshold"
    "AlarmEvaluationPeriods" = "10"
    "AlarmStatistic" = "Average"
    "AlarmThreshold" = "20"
    "AlarmPeriod" = "60"
    "NamingStandard" = "elb-latency-high-cw-alarm"
}

module "cloudwatch5" {
    source = "bitbucket.org/huitcloudservices/hcdo-tf-module-cloudwatch-alarms"
    Environment = "dev" #(dev, test, stage, prod)
    ApplicationName = ""
    ElasticLoadBalancer = ""
    CriticalAlarmTopicARN = ""
    "MetricName" = "RequestCount"
    "AlarmComparisonOperator" = "GreaterThanThreshold"
    "AlarmEvaluationPeriods" = "10"
    "AlarmStatistic" = "Sum"
    "AlarmThreshold" = "100"
    "AlarmPeriod" = "60"
    "NamingStandard" = "elb-request-count-high-cw-alarm"
}

```
